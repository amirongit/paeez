from flask import Flask, render_template
from flask_migrate import Migrate

from shop_app.utils import shop_blueprint
from user_app.utils import user_blueprint, get_role
from blog_app.utils import blog_blueprint

from blog_app.models import Post

from utils import alchemy_base

app = Flask(__name__)
app.config.from_json(filename='config.json')
alchemy_base.init_app(app)
migra_obj = Migrate(app, alchemy_base)

app.register_blueprint(shop_blueprint)
app.register_blueprint(user_blueprint)
app.register_blueprint(blog_blueprint)


@app.route('/')
def index():
    return render_template('index.html', title='خانه', role=get_role,
                           posts=Post.query.order_by(Post.pid.desc()))
