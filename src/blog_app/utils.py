from datetime import datetime

from flask import Blueprint

blog_blueprint = Blueprint('blog_blueprint', __name__, url_prefix='/blog/')


def generate_slug(title):
    return(str(datetime.now()).split()[0] + '-' +
           '-'.join(title.split()))
