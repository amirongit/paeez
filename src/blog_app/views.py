from flask import (render_template, request, redirect,
                   url_for, session, flash, abort)
from sqlalchemy.exc import IntegrityError

from user_app.utils import postperm_required, get_role, user_login_required
from user_app.models import Admin, Seller, User
from .utils import blog_blueprint, generate_slug
from .models import Post, Comment, alchemy_base


@postperm_required
def create_post():
    if request.method == 'GET':
        return render_template('blog_app/create_post.html',
                               title='پست جدید', role=get_role)

    if request.method == 'POST':
        form = request.form
        new_post = Post(ptitle=form.get('title'),
                        pcontent=form.get('content'),
                        pslug=generate_slug(form.get('title')))
        if get_role() == 'Admin':
            admin = Admin.query.get(int(session['user_id']))
            admin.admin_posts.append(new_post)
        if get_role() == 'Seller':
            seller = Seller.query.get(int(session['user_id']))
            seller.seller_posts.append(new_post)

        try:
            alchemy_base.session.add(new_post)
            alchemy_base.session.commit()
            flash('پست شما با موفقیت انتشار یافت.', category='success')
            return(redirect(url_for('index')))

        except IntegrityError:
            alchemy_base.session.rollback()
            flash('انتشار پست با خطا مواجه شد.', category='success')
            return(redirect(url_for('index')))


blog_blueprint.add_url_rule('/create_post/', 'create_post',
                            create_post, methods=['GET', 'POST'])


@blog_blueprint.route('/display_post/<slug>')
def display_post(slug):
    post = Post.query.filter(Post.pslug == slug).first()

    if post is None:
        abort(404)

    return render_template('blog_app/display_post.html',
                           title=post.ptitle, role=get_role, post=post)


@user_login_required
def submit_post_comment(postid):
    post = Post.query.get(postid)
    user = User.query.get(int(session['user_id']))
    form = request.form
    new_comment = Comment(ccontent=form.get('comment'))
    post.post_comments.append(new_comment)
    user.user_comments.append(new_comment)
    alchemy_base.session.add(new_comment)
    alchemy_base.session.commit()
    flash('نظر شما با موفقیت انتشار یافت.', category='success')
    return redirect(url_for('blog_blueprint.display_post', slug=post.pslug))


blog_blueprint.add_url_rule('/submit_post_comment/<int:postid>',
                            'submit_post_comment', submit_post_comment,
                            methods=['POST'])


@postperm_required
def list_posts():

    if get_role() == 'Admin':
        posts = Admin.query.get(int(session['user_id'])).admin_posts

    if get_role() == 'Seller':
        posts = Seller.query.get(int(session['user_id'])).seller_posts

    return render_template('blog_app/list_posts.html',
                           title='مدیریت پست ها', role=get_role, posts=posts)


blog_blueprint.add_url_rule('/list_posts/', 'list_posts', list_posts)


@postperm_required
def delete_post(postid):
    post = Post.query.get(postid)
    alchemy_base.session.delete(post)
    alchemy_base.session.commit()
    flash('پست مورد نظر با موفقیت حذف شد.', category='success')
    return redirect(url_for('blog_blueprint.list_posts'))


blog_blueprint.add_url_rule('/delete_post/<int:postid>', 'delete_post',
                            delete_post, methods=['POST'])
