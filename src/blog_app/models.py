from sqlalchemy import String, Text, Integer, Column, ForeignKey
from sqlalchemy.orm import relationship

from utils import alchemy_base


class Post(alchemy_base.Model):

    __tablename__ = 'posts'

    pid = Column(Integer, primary_key=True)
    ptitle = Column(String(64), unique=True, nullable=False)
    pcontent = Column(Text, nullable=False)
    pslug = Column(String(128), unique=True, nullable=False)
    admin_author = Column(Integer, ForeignKey('admins.aid'))
    admin_obj = relationship('Admin', back_populates='admin_posts')
    seller_author = Column(Integer, ForeignKey('sellers.sid'))
    seller_obj = relationship('Seller', back_populates='seller_posts')
    post_comments = relationship('Comment', back_populates='comment_post')


class Comment(alchemy_base.Model):

    __tablename__ = 'comments'

    cid = Column(Integer, primary_key=True)
    ccontent = Column(Text, nullable=False)
    user_id = Column(Integer, ForeignKey('users.uid'))
    comment_user = relationship('User', back_populates='user_comments')
    post_id = Column(Integer, ForeignKey('posts.pid'))
    comment_post = relationship('Post', back_populates='post_comments')
    product_id = Column(Integer, ForeignKey('products.pid'))
    comment_product = relationship('Product',
                                   back_populates='product_comments')
