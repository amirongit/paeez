from json import load
from os import path
from uuid import uuid4

from flask import request, render_template, redirect, url_for, flash, session
from sqlalchemy.exc import IntegrityError

from user_app.utils import seller_login_required, get_role, user_login_required
from user_app.models import Seller, User
from blog_app.utils import generate_slug
from blog_app.models import Comment
from .utils import shop_blueprint, handle_category
from .models import Product, alchemy_base


UPLOAD_FOLDER = load(open('config.json'))['UPLOAD_FOLDER']


@seller_login_required
def create_product():

    if request.method == 'GET':
        return render_template('shop_app/create_product.html',
                               title='محصول جدید', role=get_role)

    if request.method == 'POST':
        form = request.form

        try:
            seller = Seller.query.get(int(session['user_id']))
            picture = request.files['picture']
            category = handle_category(form.get('category'))
            new_product = Product(pname=form.get('name'),
                                  pdescription=form.get('description'),
                                  ppname=str(uuid4()) + picture.filename,
                                  pprice=form.get('price'),
                                  pslug=generate_slug(form.get('name')))
            category.category_products.append(new_product)
            seller.seller_products.append(new_product)
            alchemy_base.session.add(new_product)
            picture.save(path.join(UPLOAD_FOLDER, new_product.ppname))
            alchemy_base.session.commit()
            flash('محصول شما با موفقیت ثبت شد.', category='success')
            return redirect(url_for('index'))

        except IntegrityError:
            alchemy_base.session.rollback()
            flash('ثبت محصول با مشکل مواجه شد.', category='warning')
            return redirect(url_for('index'))


shop_blueprint.add_url_rule('/create_product/', 'create_product',
                            create_product, methods=['GET', 'POST'])


@shop_blueprint.route('/display_product/<slug>')
def display_product(slug):
    product = Product.query.filter(Product.pslug == slug).first()

    if product is None:
        abort(404)

    return render_template('shop_app/display_product.html',
                           title=product.pname, role=get_role, product=product)


@user_login_required
def submit_product_comment(productid):
    product = Product.query.get(productid)
    user = User.query.get(int(session['user_id']))
    form = request.form
    new_comment = Comment(ccontent=form.get('comment'))
    product.product_comments.append(new_comment)
    user.user_comments.append(new_comment)
    alchemy_base.session.add(new_comment)
    alchemy_base.session.commit()
    flash('نظر شما با موفقیت ثبت شد.', category='success')
    return redirect(url_for('shop_blueprint.display_product',
                    slug=product.pslug))


shop_blueprint.add_url_rule('/submit_product_comment/<int:productid>',
                            'submit_product_comment', submit_product_comment,
                            methods=['POST'])


@user_login_required
def add_to_basket(productid):
    product = Product.query.get(productid)

    if session.get('user_basket') is None:
        session['user_basket'] = list()

    user_basket = session['user_basket']
    user_basket.append(product.pid)
    session['user_basket'] = user_basket
    flash('محصول مورد نظر با موفقیت به سبد خرید اضافه شد.', category='success')
    return redirect(url_for('shop_blueprint.display_product',
                            slug=product.pslug))


shop_blueprint.add_url_rule('/add_to_basket/<int:productid>', 'add_to_basket',
                            add_to_basket, methods=['POST'])


@user_login_required
def user_basket():
    user_items = [Product.query.get(productid) for productid
                  in session['user_basket']]
    return render_template('shop_app/user_basket.html', title='سبد خرید',
                           role=get_role, user_items=user_items)


shop_blueprint.add_url_rule('/user_basket/', 'user_basket', user_basket)


@user_login_required
def remove_from_basket(productid):
    user_basket = session['user_basket']
    user_basket.remove(productid)
    session['user_basket'] = user_basket
    flash('محصول مورد نظر با موفقیت از سبد خرید شما حذف شد.',
          category='success')
    return redirect(url_for('shop_blueprint.user_basket'))


shop_blueprint.add_url_rule('/remove_from_basket/<int:productid>',
                            'remove_from_basket', remove_from_basket,
                            methods=['POST'])


@seller_login_required
def list_products():
    products = Seller.query.get(int(session['user_id'])).seller_products
    return render_template('shop_app/list_products.html',
                           title='مدیریت محصولات', role=get_role,
                           products=products)


shop_blueprint.add_url_rule('/list_products/', 'list_products',
                            list_products)


@seller_login_required
def delete_product(productid):
    product = Product.query.get(productid)
    alchemy_base.session.delete(product)
    alchemy_base.session.commit()
    flash('محصول مورد نظر با موفقیت حذف شد.', category='success')
    return redirect(url_for('shop_blueprint.list_products'))


shop_blueprint.add_url_rule('/delete_product/<int:productid>',
                            'delete_product', delete_product, methods=['POST'])


@shop_blueprint.route('/')
def shop_app_index():
    return render_template('shop_app/shop_app_index.html', title='فروشگاه',
                           role=get_role, products=Product.query.order_by(
                               Product.pid.desc()))
