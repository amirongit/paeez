from sqlalchemy import Integer, Column, String, Text, ForeignKey
from sqlalchemy.orm import relationship

from utils import alchemy_base


class Category(alchemy_base.Model):

    __tablename__ = 'categories'

    cid = Column(Integer, primary_key=True)
    cname = Column(String(128), unique=True, nullable=False)
    category_products = relationship('Product',
                                     back_populates='product_category')


class Product(alchemy_base.Model):

    __tablename__ = 'products'

    pid = Column(Integer, primary_key=True)
    pname = Column(String(64), unique=True, nullable=False)
    pdescription = Column(Text, nullable=True)
    ppname = Column(String(64), unique=True)
    pslug = Column(String(128), unique=True, nullable=False)
    pprice = Column(Integer, nullable=False)
    category_id = Column(Integer, ForeignKey('categories.cid'))
    product_category = relationship('Category',
                                    back_populates='category_products')
    seller_id = Column(Integer, ForeignKey('sellers.sid'))
    product_seller = relationship('Seller', back_populates='seller_products')
    product_comments = relationship('Comment',
                                    back_populates='comment_product')
