from flask import Blueprint

from .models import Category, alchemy_base

shop_blueprint = Blueprint('shop_blueprint', __name__, url_prefix='/shop/')


def handle_category(category):
    query = Category.query.filter(Category.cname == category).first()

    if query is None:
        new_category = Category(cname=category)
        alchemy_base.session.add(new_category)
        alchemy_base.session.commit()
        query = Category.query.filter(Category.cname == category).first()

    return query
