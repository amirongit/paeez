from flask import Blueprint, session, abort

from .models import Seller, User, Admin

user_blueprint = Blueprint('user_blueprint', __name__, url_prefix='/usr/')


def get_role():
    role = session.get('user_role')
    return role if role is not None else False


def postperm_required(endpoint):
    def decorator(*args, **kwargs):

        if session.get('user_role') == 'Admin':
            return endpoint(*args, **kwargs)

        if session.get('user_role') == 'Seller' and Seller.query.get(
                int(session['user_id'])).spostperm is True:
            return endpoint(*args, **kwargs)

        abort(403)
    return decorator


def seller_login_required(endpoint):
    def decorator(*args, **kwargs):
        if session.get('user_role') != 'Seller':
            abort(403)
        return endpoint(*args, **kwargs)
    return decorator


def admin_login_required(endpoint):
    def decorator(*args, **kwargs):
        if session.get('user_role') != 'Admin':
            abort(403)
        return endpoint(*args, **kwargs)
    return decorator


def user_login_required(endpoint):
    def decorator(*args, **kwargs):
        if session.get('user_role') != 'User':
            abort(403)
        return endpoint(*args, **kwargs)
    return decorator


def login_required(endpoint):
    def decorator(*args, **kwargs):
        return endpoint(*args, **kwargs) if get_role() else abort(403)
    return decorator
