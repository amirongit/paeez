from sqlalchemy import Integer, Column, String, Text, Boolean
from sqlalchemy.orm import relationship
from werkzeug.security import check_password_hash

from utils import alchemy_base


class Admin(alchemy_base.Model):

    __tablename__ = 'admins'

    aid = Column(Integer, primary_key=True)
    aname = Column(String(64), unique=True, nullable=False)
    aemail = Column(String(128), unique=True, nullable=False)
    apasswd = Column(String(128), nullable=False)
    admin_posts = relationship('Post', back_populates='admin_obj')

    def passwd_check(self, passwd):
        return check_password_hash(self.apasswd, passwd)


class Seller(alchemy_base.Model):

    __tablename__ = 'sellers'

    sid = Column(Integer, primary_key=True)
    sname = Column(String(64), unique=True, nullable=False)
    semail = Column(String(128), unique=True, nullable=False)
    sdescription = Column(Text, nullable=False)
    spostperm = Column(Boolean, unique=False, default=False)
    spasswd = Column(String(128), nullable=False)
    seller_products = relationship('Product', back_populates='product_seller')
    seller_posts = relationship('Post', back_populates='seller_obj')

    def passwd_check(self, passwd):
        return check_password_hash(self.spasswd, passwd)


class User(alchemy_base.Model):

    __tablename__ = 'users'

    uid = Column(Integer, primary_key=True)
    uname = Column(String(64), nullable=False)
    uemail = Column(String(128), nullable=False, unique=True)
    upasswd = Column(String(128), nullable=False)
    user_comments = relationship('Comment', back_populates='comment_user')

    def passwd_check(self, passwd):
        return check_password_hash(self.upasswd, passwd)
