from flask import (session, flash, redirect, url_for,
                   request, render_template, abort)
from sqlalchemy.exc import IntegrityError
from werkzeug.security import generate_password_hash

from .models import Admin, Seller, User, alchemy_base
from .utils import (user_blueprint, get_role,
                    login_required, admin_login_required)

# TODO: USER BASKET


@admin_login_required
def list_sellers():
    return render_template('user_app/list_sellers.html',
                           title='مدیریت فروشنده ها', role=get_role,
                           sellers=list(Seller.query.all()))


user_blueprint.add_url_rule('/list_sellers/', 'list_sellers', list_sellers)


@admin_login_required
def remove_seller(sid):
    seller = Seller.query.get(sid)
    alchemy_base.session.delete(seller)
    alchemy_base.session.commit()
    flash('حساب کاربری ' + seller.sname +
          ' با موفقیت حذف شد.', category='success')
    return redirect(url_for('user_blueprint.list_sellers'))


user_blueprint.add_url_rule('/remove_seller/<int:sid>', 'remove_seller',
                            remove_seller, methods=['POST'])


@admin_login_required
def change_postperm(sid):
    seller = Seller.query.get(sid)

    if seller.spostperm:
        print('FUCK')
        seller.spostperm = False
        alchemy_base.session.commit()
        flash('مجوز پست از کاربر ' + seller.sname +
              ' گرفته شد.', category='success')
        return redirect(url_for('user_blueprint.list_sellers'))

    if not seller.spostperm:
        seller.spostperm = True
        alchemy_base.session.commit()
        flash('مجوز پست به کاربر ' + seller.sname +
              ' داده شد.', category='success')
        return redirect(url_for('user_blueprint.list_sellers'))


user_blueprint.add_url_rule('/change_postperm/<int:sid>', 'change_postperm',
                            change_postperm, methods=['POST'])


@user_blueprint.route('/signup/user/', methods=['GET', 'POST'])
def user_signup():

    if get_role():
        abort(403)

    if request.method == 'GET':
        return render_template('user_app/user_signup.html', title='ثبت نام',
                               role=get_role)

    if request.method == 'POST':
        form = request.form

        try:
            new_user = User(uname=form.get('name'), uemail=form.get('email'),
                            upasswd=generate_password_hash(
                                form.get('password')))
            alchemy_base.session.add(new_user)
            alchemy_base.session.commit()
            flash('ثبت نام شما با موفقیت انجام شد.', category='success')
            return redirect(url_for('index'))

        except IntegrityError:
            alchemy_base.session.rollback()
            flash('مشخصات شما قبلا در سیستم وارد شده است.', category='warning')
            return render_template('user_app/user_signup.html',
                                   title='ثبت نام', role=get_role)


@user_blueprint.route('signup/seller/', methods=['GET', 'POST'])
def seller_signup():

    if get_role():
        abort(403)

    if request.method == 'GET':
        return render_template('user_app/seller_signup.html', title='ثبت نام',
                               role=get_role)

    if request.method == 'POST':
        form = request.form

        try:
            new_seller = Seller(sname=form.get('name'),
                                semail=form.get('email'),
                                sdescription=form.get('description'),
                                spasswd=generate_password_hash(
                                    form.get('password')))
            alchemy_base.session.add(new_seller)
            alchemy_base.session.commit()
            flash('ثبت نام شما با موفقیت انجام شد.', category='success')
            return redirect(url_for('index'))

        except IntegrityError:
            alchemy_base.session.rollback()
            flash('مشخصات شما قبلا در سیستم وارد شده است.', category='warning')
            return render_template('user_app/seller_signup.html',
                                   title='ثبت نام', role=get_role)


@user_blueprint.route('/login/', methods=['GET', 'POST'])
def login():

    if get_role():
        abort(403)

    if request.method == 'GET':
        return render_template('user_app/login.html', title='ورود',
                               role=get_role)

    if request.method == 'POST':
        form = request.form

        if form.get('role') == 'Admin':
            query = Admin.query.filter(
                    Admin.aemail == form.get('email')).first()

            if query is not None:
                ses_id = query.aid

        if form.get('role') == 'Seller':
            query = Seller.query.filter(
                    Seller.semail == form.get('email')).first()

            if query is not None:
                ses_id = query.sid

        if form.get('role') == 'User':
            query = User.query.filter(
                    User.uemail == form.get('email')).first()

            if query is not None:
                ses_id = query.uid

        if query is not None and query.passwd_check(form.get('password')):
            session['user_role'] = form.get('role')
            session['user_id'] = ses_id
            flash('شما با موفقیت وارد شدید.', category='success')
            return redirect(url_for('index'))

        else:
            flash('مشخصات به درستی وارد نشده است.', category='warning')
            return render_template('user_app/login.html',
                                   title='ورود', role=get_role)


@login_required
def logout():
    session.clear()
    flash('شما با موفقیت خارج شدید.', category='success')
    return redirect(url_for('index'))


user_blueprint.add_url_rule('/logout/', 'logout', logout)
