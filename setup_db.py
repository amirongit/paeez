from subprocess import call
from time import sleep

call('flask db init', shell=True)
sleep(2)
call('flask db stamp head', shell=True)
sleep(2)
call('flask db migrate', shell=True)
sleep(2)
call('flask db upgrade', shell=True)

# cd to src folder and run this script to setup ur database.
